Candy Syntax Extension for Mecha
================================

Release Notes
-------------

### 1.5.1

 - Make candy syntax example becomes copy-paste friendly.
